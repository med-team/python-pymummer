python-pymummer (0.11.0-5) unstable; urgency=medium

  * Team upload.
  * Patch-out undeclared usage of pkg_resources
  * Standards-Version: 4.7.0

 -- Alexandre Detiste <tchet@debian.org>  Fri, 21 Feb 2025 22:29:47 +0100

python-pymummer (0.11.0-4) unstable; urgency=medium

  * Team upload.
  * Packaging update
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * d/rules: Remove override_dh_auto_clean and let dh-sequence-python3
    clean .egg-info/ (Closes: #1046452)
  * d/rules: Replace override_dh_auto_test with PYBUILD variables
  * d/control: Replace autopkgtest-pkg-python with autopkgtest-pkg-pybuild
  * d/tests: Removed, run upstream tests with autopkgtest-pkg-pybuild

 -- Lance Lin <lq27267@gmail.com>  Mon, 04 Dec 2023 20:52:14 +0700

python-pymummer (0.11.0-3) unstable; urgency=medium

  * Team upload.
  * Update standards version to 4.6.1, no changes needed.
  * Update watch file format version to 4.
  * d/watch: move to git mode instead of web scraping; in both cases we scan
    for git tags, so git mode is probably more robust.
  * d/t/run-unit-test: run against all supported python3 versions.
  * d/copyright: update debian maintainers over the years.
  * d/control: switch from nose to pytest.
  * d/rules: invoke pytest instead of setup.py.
    Closes: #1018565

 -- Étienne Mollier <emollier@debian.org>  Sat, 03 Dec 2022 17:22:28 +0100

python-pymummer (0.11.0-2) unstable; urgency=medium

  * Team Upload.
  * Add autopkgtests

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 28 Aug 2020 20:28:54 +0530

python-pymummer (0.11.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse. (routine-update)
  * lintian complained about missing maintainer, added Andreas and myself
  * removed egg.info files

 -- Steffen Moeller <moeller@debian.org>  Wed, 10 Jun 2020 02:13:58 +0200

python-pymummer (0.10.3-2) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target

 -- Andreas Tille <tille@debian.org>  Mon, 07 Jan 2019 09:37:20 +0100

python-pymummer (0.10.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 29 Jan 2018 15:46:12 +0100

python-pymummer (0.10.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 02 Feb 2017 14:30:00 +0100

python-pymummer (0.10.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 18 Nov 2016 19:58:54 +0000

python-pymummer (0.10.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 15 Nov 2016 11:18:13 +0000

python-pymummer (0.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 18 Oct 2016 18:37:05 +0000

python-pymummer (0.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 24 Aug 2016 13:45:50 +0000

python-pymummer (0.7.1-1) unstable; urgency=low

  * Update Standards-Version
  * Imported Upstream version 0.7.1

 -- Afif Elghraoui <afif@debian.org>  Sat, 23 Apr 2016 20:25:13 -0700

python-pymummer (0.7.0-1) unstable; urgency=medium

  * Imported Upstream version 0.7.0
  * Use secure VCS URLs
  * Bump copyright year

 -- Afif Elghraoui <afif@ghraoui.name>  Fri, 12 Feb 2016 01:09:47 -0800

python-pymummer (0.6.1-1) unstable; urgency=low

  * Initial release (Closes: #805230)

 -- Afif Elghraoui <afif@ghraoui.name>  Sun, 15 Nov 2015 15:00:31 -0800
